package SSCA2;

public class Globals {

    public Globals() {
    }

    public int SCALE;

    public int TOT_VERTICES;

    public int MAX_CLIQUE_SIZE;

    public int MAX_PARAL_EDGES;

    public int MAX_INT_WEIGHT;

    public byte[] SOUGHT_STRING;

    public int MAX_STRLEN;

    public float PERC_INT_WEIGHTS;

    public float PROB_UNIDIRECTIONAL;

    public float PROB_INTERCL_EDGES;

    public int SUBGR_EDGE_LENGTH;

    public int MAX_CLUSTER_SIZE;

    public int K3_DS;

    public int THREADS;
}
