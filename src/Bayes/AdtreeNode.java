package Bayes;

public class AdtreeNode {

    int index;

    int value;

    int count;

    AdtreeVary varyVectorPtr[];

    public AdtreeNode() {
    }

    public AdtreeNode(int index, int vecsize) {
        this.varyVectorPtr = new AdtreeVary[vecsize];
        this.index = index;
        this.value = -1;
        this.count = -1;
    }
}
